# CPPTools

This repository contains mainly docker images required for the C++ course.

## Usage

### Get docker image
Login to the school's gitlab docker container repository:

    docker login registry.forge.hefr.ch

Download the docker image locally with:

    docker pull registry.forge.hefr.ch/beatwolf/cppbachelor/cpptools:latest

On Windows you can also use the Docker Desktop application:

![docker_desktop.png](docker_desktop.png)

### Integrate into CLion
Add this docker image as the run environment to CLion:

![clion-settings.png](clion-settings.png)

Make sure that the docker toolchain is the top (default) toolchain.

You also have to tell CLion where/how to find valgrind. Simply type valgrind in the required settings field.

![valgrind.png](valgrind.png)

### Google Perf Tools
When profiling an application inside a docker container with clion, the profiling file will be visible outside the docker container.
But to run the transformation into a PDF file, you need the google-pprof, and the ability to run it on the same platform as the one on which the profiling was done.
As of today, there is no straight forward way to open a terminal in docker, with the current project loaded, from inside clion.
The following one line command will generate the PDF of the profiling. *BUT YOU NEED TO ADAPT THE COMMAND*

    docker run -rm -v C:\Users\beat.wolf\CLionProjects\cppalgo\cmake-build-debug:/build registry.forge.hefr.ch/beatwolf/cppbachelor/cpptools:latest sh -c 'google-pprof --pdf /build/benchmark /build/perf.prof > /build/profiling.pdf'

If you want to run a terminal in docker with your project loaded, use the following command:

    docker run -rm -v C:\Users\beat.wolf\CLionProjects\cppalgo\cmake-build-debug:/build registry.forge.hefr.ch/beatwolf/cppbachelor/cpptools:latest bash

## FAQ
### 1) How can i update my image?

Just run the docker pull command from above again.

### 2) I'm lost or would like to do more
Checkout out the official CLion tutorial video:

<https://www.youtube.com/watch?v=p7Bi-mOyelM>

## 3) I get an authentication error when pulling the image
Make sure you are correctly logged using docker login. Repeat the login procedure if you recently updated your password.