FROM ubuntu:20.04

#Add up to date cmake repository to ensure compatbility with all tools
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install ca-certificates gpg wget  --no-install-recommends -y \
  && apt-get clean

RUN test -f /usr/share/doc/kitware-archive-keyring/copyright \
  || wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /usr/share/keyrings/kitware-archive-keyring.gpg >/dev/null \
  && echo 'deb [signed-by=/usr/share/keyrings/kitware-archive-keyring.gpg] https://apt.kitware.com/ubuntu/ focal main' | tee /etc/apt/sources.list.d/kitware.list >/dev/null

#Install all required packages
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    tzdata \
      build-essential \
      gdb \
      valgrind \
      cmake \
      libgoogle-perftools-dev \
      graphviz \
      ghostscript \
      google-perftools \
      python3 \
      python3-dev \
      python3-distutils \
      python-is-python3 \
      python3-pip \
      git \
      nano \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

#Install python dependencies
RUN python -m pip install --upgrade pip
RUN pip --version
RUN pip install --no-cache-dir pybind11[global] mypy intervaltree conan
